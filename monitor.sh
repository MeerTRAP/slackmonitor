#!/bin/bash

# TEST CHANNEL
#curl -X POST -H "Content-type: application/json" --data "$json" https://hooks.slack.com/services/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

# TUSE CHANNEL
#curl -X POST -H "Content-type: application/json" --data "$json" https://hooks.slack.com/services/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

#test_link="https://hooks.slack.com/services/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"
#tuse_link="https://hooks.slack.com/services/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"
#monitor_link="https://hooks.slack.com/services/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"

source destinations.sh

use_link=$test_link

info_limit=50.0
warn_limit=10.0
crit_limit=1.0

declare -a INFO_SENT
declare -a WARN_SENT
declare -a CRIT_SENT

for node in {0..66}
do
    INFO_SENT[$node]=0
    WARN_SENT[$node]=0
    CRIT_SENT[$node]=0
done

while true
do

declare -a INFO_SIZE
declare -a INFO_NODE

declare -a WARN_SIZE
declare -a WARN_NODE

declare -a CRIT_SIZE
declare -a CRIT_NODE

declare -a CLEAN_SIZE
declare -a CLEAN_NODE

info_count=0
warn_count=0
crit_count=0
clean_count=0

IFS='
'

cur_time=$( date +'%Y-%m-%d %H:%M:%S' )

for result in $(rocks run host "df /state/partition1 -h | sed -n '2p'" collate=true | awk '{print $5,$1}')
do
    size_left=$(awk '{print $1}' <<< $result | sed 's/.$//')
    node=$(awk '{print $2}' <<< $result | sed 's/.$//')
    node_number=$( echo $node | cut -c 7- ) 

    if [[ "$size_left" != "tpn-0-49" ]]
    then

    if (( $(echo "$size_left < $info_limit" | bc -l) && $(echo "$size_left > $warn_limit" | bc -l) && $(echo "${INFO_SENT[${node_number}]} == 0" | bc -l) )) 
    #if [ "$size_left" -le "$info_limit" ] && [ "$size_left" -gt "$warn_limit" ] && [ "${INFO_SENT[${node_number}]}" -eq 0 ]
    then
        INFO_NODE[${info_count}]=${node}
        INFO_SIZE[${info_count}]=${size_left}
        INFO_SENT[${node_number}]=1
        info_count=$((info_count+1))

        WARN_SENT[${node_number}]=0
        CRIT_SENT[${node_number}]=0
    
    elif (( $(echo "$size_left < $warn_limit" | bc -l) && $(echo "$size_left > $crit_limit" | bc -l) && $(echo "${WARN_SENT[${node_number}]} == 0" | bc -l) )) 
    #[ "$size_left" -le "$warn_limit" ] && [ "$size_left" -gt "$crit_limit" ] && [ "${WARN_SENT[${node_number}]}" -eq 0 ]
    then
        WARN_NODE[${warn_count}]=${node}
        WARN_SIZE[${warn_count}]=${size_left}
        warn_count=$((warn_count+1))
        WARN_SENT[${node_number}]=1

        INFO_SENT[${node_number}]=0
        CRIT_SENT[${node_number}]=0

    elif (( $(echo "$size_left < $crit_limit" | bc -l) && $(echo "${CRIT_SENT[${node_number}]} == 0" | bc -l) ))
    #[ "$size_left" -le "$crit_limit" ] && [ "${CRIT_SENT[${node_number}]}" -eq 0 ]
    then 
        CRIT_NODE[${crit_count}]=${node}
        CRIT_SIZE[${crit_count}]=${size_left}
        CRIT_SENT[${node_number}]=1
        crit_count=$((crit_count+1))

        INFO_SENT[${node_number}]=0
        CRIT_SENT[${node_number}]=0

    elif (( $(echo "$size_left > $info_limit" | bc -l) && $(echo "${INFO_SENT[${node_number}]} == 1" | bc -l) && $(echo "${WARN_SENT[${node_number}]} == 1" | bc -l) && $(echo "${CRIT_SENT[${node_number}]} == 1" | bc -l) ))
    #[[ ( "$size_left" -gt "$info_limit" ) && ( ( "${INFO_SENT[${node_number}]}" -eq 1 ) || ( "${WARN_SENT[${node_number}]}" -eq 1 ) || ( "${CRIT_SENT[${node_number}]}" -eq 1 ) ) ]]
    then

        INFO_SENT[${node_number}]=0
        WARN_SENT[${node_number}]=0
        CRIT_SENT[${node_number}]=0

        CLEAN_NODE[${clean_count}]=${node}
        CLEAN_SIZE[${clean_count}]=${size_left}

        clean_count=$((clean_count+1))
    fi 
    fi
done


list_info=""
list_warn=""
list_crit=""
list_clean=""
json=""
json_info="{}"
json_warn="{}"
json_crit="{}"
json_clean="{}"

if [ "$info_count" -gt 0 ]
then
    for info in $( seq 0 $((info_count - 1)) )
    do
        list_info="${list_info}Node ${INFO_NODE[$info]} has ${INFO_SIZE[$info]}GB left\n"
    done

    json_info="{ \
                \"pretext\": \"*${cur_time} IMPORTANT:* $info_count nodes have less than ${info_limit}GB left\", \
                \"color\": \"#eb8f34\", \
                \"text\": \"$list_info\" \
            } "
fi

if [ "$warn_count" -gt 0 ]
then
    for info in $( seq 0 $((warn_count - 1)) )
    do
        list_warn="${list_warn}Node ${WARN_NODE[$info]} has ${WARN_SIZE[$info]}GB left\n"
    done

    json_warn="{ \
                \"pretext\": \"*${cur_time} WARNING:* $warn_count nodes have less than ${warn_limit}GB left\", \
                \"color\": \"#c91e1e\", \
                \"text\": \"$list_warn\" \
            } "
fi

if [ "$crit_count" -gt 0 ]
then
    for info in $( seq 0 $((crit_count - 1)) )
    do
        list_crit="${list_crit}Node ${CRIT_NODE[$info]} has ${CRIT_SIZE[$info]}GB left\n"
    done

    json_crit="{ \
                \"pretext\": \"*${cur_time} CRITICAL:* $crit_count nodes have less than ${crit_limit}GB left\", \
                \"color\": \"#363533\", \
                \"text\": \"$list_crit\" \
            } "
fi

if [ "$clean_count" -gt 0 ]
then 

    for clean in $( seq 0 $((clean_count - 1)) )
    do
        list_clean="${list_clean}Node ${CLEAN_NODE[$clean]} has ${CLEAN_SIZE[$clean]}GB left\n"
    done

    json_clean="{ \
                \"pretext\": \"*${cur_time} CLEANED:* $clean_count nodes now have more than ${info_limit}GB left\", \
                \"color\": \"#37961d\", \
                \"text\": \"$list_clean\" \
            } "
fi

# Need to clean up for when we have empty warning and critical
json="{   
       \"attachments\": [
       $json_info, $json_warn, $json_crit, $json_clean
       ]
      }"

#echo $json

# That should really be called only when we have something to send
curl -X POST -H "Content-type: application/json" --data "$json" $use_link

sleep 60s

done
